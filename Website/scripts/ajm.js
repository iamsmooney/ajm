/* Ca-Phun Ung < caphun at tofugear dot com>
 * Support for fullscreen horizontal scroll transitions.
 */
$.fn.cycle.transitions.scrollMaxHoriz = function($cont, $slides, opts) {
    var w = $(window).width();
    var h = $(window).height();
    if (opts.minWidth && w < opts.minWidth) w = opts.minWidth;
    if (opts.minHeight && h < opts.minHeight) h = opts.minHeight;
  opts.before.push(function(curr, next, opts, fwd) {
    if (opts.rev)
      fwd = !fwd;
        curr.cycleW = next.cycleW = w;
        curr.cycleH = next.cycleH = h;
    $.fn.cycle.commonReset(curr,next,opts);
    opts.cssBefore.left = fwd ? (next.cycleW-1) : (1-next.cycleW);
    //opts.animOut.left = fwd ? -curr.cycleW : curr.cycleW;
  });
  opts.cssFirst.left = 0;
  opts.cssBefore.top = 0;
  opts.animIn.left = 0;
  opts.animOut.top = 0;
};

var ss12_visible = true;
var ss12_hover = false;
var index_hover = false;
var press_hover = false;

var replaceScrollbar = false;
var isResizing = false;

$(window).resize(function() {
	//$('.slide').css('width','100%').css('height','100%');
	//$('#gallery').css('width','100%').css('height','100%');
	$('.slide').css({
		'width': $(window).width(),
		'height': $(window).height()
	});
	$('#container > div').css({
		'width': $(window).width(),
		'height': $(window).height()
	});
	$('#gallery').css({
		'width': $(window).width(),
		'height': $(window).height()
	});
	$('.audio').css({
		'top': $(window).height() - 55
	});
	$('#gallery_prev').css({
		'top': $(window).height() / 3
	});
	$('#gallery_next').css({
		'top': $(window).height() / 3
	});
	
	/*
	rows:
	182 164
	398 380
	614 594
	
	cols:
	158
	358
	558
	*/
	if ($('#press').width() >= 944) {
		$('.pb1').css({
			'top':164,
			'left':158
		});
		$('.pb2').css({
			'top':164,
			'left':358
		});
		$('.pb3').css({
			'top':164,
			'left':558
		});
		
		$('.pb4').css({
			'top': 380,
			'left': 158
		});
		$('.pb5').css({
			'top': 380,
			'left': 358
		});
		$('.pb6').css({
			'top': 380,
			'left': 558
		});
	} else if ($('#press').width() >= 717) {
		$('.pb1').css({
			'top':164,
			'left':158
		});
		$('.pb2').css({
			'top':164,
			'left':358
		});
		
		$('.pb3').css({
			'top':380,
			'left':158
		});
		$('.pb4').css({
			'top': 380,
			'left': 358
		});
		
		$('.pb5').css({
			'top': 594,
			'left': 158
		});
		$('.pb6').css({
			'top': 594,
			'left': 358
		});
	} else if ($('#press').width() >= 492) {
		$('.pb1').css({
			'top':164,
			'left':158
		});
		$('.pb2').css({
			'top':380,
			'left':158
		});
		
		$('.pb3').css({
			'top':594,
			'left':158
		});
		$('.pb4').css({
			'top': 806,
			'left': 158
		});
		
		$('.pb5').css({
			'top': 1020,
			'left': 158
		});
		$('.pb6').css({
			'top': 1310,
			'left': 158
		});
	} else {
		$('.press_between').css('left',-300);
	}
	
	if (!isResizing && replaceScrollbar) {
		isResizing = true;
		var container = $('#full-page-container');
		// Temporarily make the container tiny so it doesn't influence the
		// calculation of the size of the document
		container.css(
			{
				'width': 1,
				'height': 1
			}
		);
		// Now make it the size of the window...
		container.css(
			{
				'width': $(window).width(),
				'height': $(window).height()
			}
		);
		isResizing = false;
		container.jScrollPane(
			{
				'showArrows': true
			}
		);
	}
});

function ss12box(u,t) {
	$("#ss12_box .content .body .videoimg").attr('src',u);
	$("#ss12_box .content .body .videodescription").text(t);
	$("#ss12_box").show();
	if (soundManager.getSoundById('basicMP3Sound0')) {
		fadeOutSound('basicMP3Sound0',-5);
	}
	$("#ss12_box .close").click(function() {
		$("#ss12_box").hide();
	});
	$("#ss12_box").click(function() {
		if (!ss12_hover) {
			$("#ss12_box").hide();
		}
	});
}

function indexOpen() {
	$("#index").css({
		"display":"block",
		"height":$(document).height()
		}
	);
	$("#index_body").css({
		"top":$(document).scrollTop()
		}
	);
	
	$("#index .close").click(function() {
		indexClose(-1);
	});
}

function social() {
	$("#social_box").show();
	$("#social_box .close").click(function() {
		$("#social_box").hide();
	});
}

function indexClose(t) {
	$("#index").css("display","none");
	if (t == "about") {
		$.scrollTo('#about');
	} else if (t == "press") {
		$.scrollTo('#press');
	} else if (t == "contact") {
		$.scrollTo('#contact');
	} else if (t > 0) {
		$('#gallery').cycle(t,'scrollMaxHoriz');
	}
}

function pressBox(u,t) {
	$("#press_box .content .body .pressimg").attr('src',u);
	//$("#press_box .content .body .pressdescription").text(t);
	$("#press_box .content .body .pressdescription").html(t);
	$(".pressimg").css(
			{
				'height': $(window).height() - 150
			}
		);
	$("#press_box").show();
	$("#press_box .close").click(function() {
		$("#press_box").hide();
	});
	$("#press_box").click(function() {
		if (!press_hover) {
			$("#press_box").hide();
		}
	});
}

$(document).ready(function() {
	startCycle();
    
	$('#navigation').onePageNav({
	  currentClass: 'selected',
	  changeHash: true,
	  scrollSpeed: 750,
	  scrollOffset: 0
	});
	
	// Workaround for known Opera issue which breaks demo (see
	// http://jscrollpane.kelvinluck.com/known_issues.html#opera-scrollbar )
	if (replaceScrollbar) {
		$('body').css('overflow', 'hidden');
		$('#container').css('overflow','hidden');
	}
	// IE calculates the width incorrectly first time round (it
	// doesn't count the space used by the native scrollbar) so
	// we re-trigger if necessary.
	if ($('#full-page-container').width() != $(window).width()) {
		$(window).trigger('resize');
	}
	
	$('#ss12_body').hover(function(){ 
        ss12_hover=true; 
    }, function(){ 
        ss12_hover=false; 
    });
    $('#index_body').hover(function(){ 
        index_hover=true; 
    }, function(){ 
        index_hover=false; 
    });
	$('#press_body').hover(function(){ 
        press_hover=true; 
    }, function(){ 
        press_hover=false; 
    });
    
    $('#nav_lookbook').click(function() {
    	$('#gallery').cycle(0,'none');
    	$('#gallery').cycle(0,'scrollMaxHoriz');
    	return false;
    });
    $('#logo > a > img').click(function() {
    	$('#gallery').cycle(0,'none');
    	$('#gallery').cycle(0,'scrollMaxHoriz');
    	$.scrollTo('#lookbook');
    	return false;
    });
    
	$(window).trigger('resize');
	
	$(".social").click(function() {
		social();
	});
	
	$("#nav_index").click(function() {
		indexOpen();
	});
	$("a.press_item").click(function() {
		//console.log($(this).children("div"));
		var u = $(this).next("div").children(".press_item_image").text();
		var t = $(this).next("div").children(".press_item_body").html();
		//console.log(t);
		pressBox(u,t);
	});
});

function startCycle() {
	var index = 0, hash = window.location.hash;
	/*if (hash != "lookbook"
		&& hash != "about"
		&& hash != "press"
		&& hash != "contact") {
		index = /\d+/.exec(hash)[0];
		index = (parseInt(index) || 1) - 1; // slides are zero-based
	}*/

	$('#gallery').cycle({ 
		fx: 	 'scrollMaxHoriz',
		timeout: 3000, 
		delay:	 0,
		speed:   1600,
		startingSlide: index,
		prev:	 '#gallery_prev',
		next:	 '#gallery_next',
		cssBefore: {  
			width: '100%', 
			height: '100%'
		},
		after: function(curr,next,opts) {
			window.location.hash = opts.currSlide + 1;
		}

	});
	$('#gallery').cycle('pause');
	
    var h = $(window).height();
    $('#gallery').css('height',h);
    
}